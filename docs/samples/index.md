# FTAG Samples

The algorithm training and evaluations are based on simulated event samples.

While the corresponding samples for the Run-2 dataset are based on the release 21 reprocessed MC, there are new simulations for the Run-3 dataset.
Further details are provided on the MC production campaign TWiki pages:

- [MC20 production campaign TWiki](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AtlasProductionGroupMC20)
- [MC23a production campaign TWiki](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AtlasProductionGroupMC23a)
- [MC23c production campaign TWiki](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AtlasProductionGroupMC23c) **being replaced by MC23d**
- [MC23d production campaign TWiki](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AtlasProductionGroupMC23d) 

Most studies within the FTAG group make use of DxAOD.
More information about different FTAG derivation formats can be found in the [here](daod.md).

## Tags

Information about each datasets is provided in its tag, which is part of the dataset name.
A tag typically consists of three elements:

- e-tag: provides information about the event generation stage
- a-tag or s-tag: provides information about the simulation stage (for fast or full Geant4 detector simulation, respectively)
- r-tag: provides information about the low-level reconstruction stage.
- p-tag: provides information about the derivation stage (higher levle reconstruction).

In r22, the default simulation configuration includes re-simulating long living particles. Therefore, the standard s-tag for the mc20 samples in r22 is `s3681` (while for r21 it was `s3126`). 
For reference, the correspondence of the r-tags to the different MC campaigns is reproduced below.

(Note: `mc23d` should be used instead of `mc23c`, if available. The only difference between `mc23d` and `mc23c` is the effective statistic. In `mc23d` it is highly improved after pile-up reweighting.)

| MC campaign | r-tag  | a-tag  | s-tag  | 
| ----------- | ------ | ------ | ------ |
| mc23a       | r14622 | ?      | s4162  |
| mc23c       | r14799 | ?      | s4159  |
| mc23d       | r15224 | ?      | s4159  |
| mc20a       | r13167 | a907   | s3681  |
| mc20d       | r13144 | a907   | s3681  |
| mc20e       | r13145 | a907   | s3681  |



???+ bug "Buggy datasets"

    Certain iterations are not suitable for flavour tagging studies.

    - Avoid mc23a with the s-tag `s4111`. It misses the configuration of the ZeroLifetimePositioner that sets a proper decay length for oscillating B0 mesons. See [ATLPHYSVAL-943](https://its.cern.ch/jira/browse/ATLPHYSVAL-943).
    - Avoid fast simulation with the a-tag `a899`. It misses pre-includes for ensuring EM physics for all particles is considered in the simulation. See [ATLASSIM-6556](https://its.cern.ch/jira/browse/ATLASSIM-6556).


## Dataset Bookkeeping

The sample lists for the FTAG algorithms group are maintained using a [gitlab project](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/docs-infrastructure/atlas-ftag-management) which contains the sample lists as `yaml` files.

These lists are then used to automatically generate the [samples page](samples.md).
