# AOD Production Requests

!!! warning "this page is a work in progress"
    See [the FTAG MC request twiki](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/FTAGMCRequests) for details on making a request.


To request production (with new DSIDs or extension), one should, in general, create a ticket under [ATLMCPROD](https://its.cern.ch/jira/browse/ATLMCPROD), containing necessary information as under:

- Motivation
- DSID of requested samples. New joboptions should be registered **after validation** to get the DSID number.
- Number of events requested in each period (multiple of 10k)
- Special reconstruction/simulation requirement (specific `tag`), if any. Default will be fullsim with the latest official tag.
- FTAG approved priority. Default is 2, which means "normal".


A tutorial on how to make such requests can be found [here](https://indico.cern.ch/event/1385465/contributions/5824415/attachments/2804668/4896397/Status%20Summay%20of%20FTAG%20MC%20Request.pdf) .

Example requests:

- [https://its.cern.ch/jira/browse/ATLMCPROD-10827](https://its.cern.ch/jira/browse/ATLMCPROD-10827)

- [https://its.cern.ch/jira/browse/ATLMCPROD-10844](https://its.cern.ch/jira/browse/ATLMCPROD-10844?jql=project%20%3D%20ATLMCPROD%20AND%20component%20%3D%20Flavour-Tagging)


??? abstract "Template"

    
    Feel free to leave `<FTAG_MC_contacts>` blank if you're not sure who your FTAG MC contacts are and they would be happy to help!

    Dear MC Production Team,

    We would like to make a request for `<samples>` samples for `<usage>` in FTAG group. The stats requested are MC20(..)+MC23(..) in total `<total_stats>`. The conveners agreed to proceed with submitting the request with priority `<priority>`.

    Physics process and explanation: ....

    Generators used: ....

    Production type: ....

    Link to validation plots or presentation: ....

    Athena release: ....

    Priority and justification: ....
 
    JOs(if any newly registered): ....

    Thank you,

    `<Your name>`

    Attn: `<FTAG_MC_contacts>`

    cc: `<interested_parties>`, `<FTAG_conveners>`

