# Derivations

The FTAG group maintains several different derivation formats (DAODs) for algorithm training and various performance studies.
Different formats are specialized for different "performance study" objectives, your target objective dictates the format to be used/requested.
More details in the tables below.

??? tip "You can examine the contents of each FTAG derivation using the [nightly ART tests](https://atlas-art-data.web.cern.ch/atlas-art-data/grid-output/main/Athena/x86_64-el9-gcc13-opt/)"

    Pick a nightly based on the date, then navigate to `DerivationFrameworkFlavourTagART` and then to the derivation format you are interested in.
    There, you will find the output of a `checkFile` command, which will give you a list of the contents of the derivation.

    Note that the ART tests are run on the `main` branch, so the contents may differ from previous releases.

## FTAG Derivation Formats

??? question "Which derivation format should I use?"

    First of all, you might not want `FTAG` derivations at all:

    - Most of the flavor tagging calibrations should be using `DAOD_PHYS`, unless otherwise stated below.
    - No physics analysis should be using `FTAG` derivaitons.

    Pay attention to the purpose: if your use case isn't explicitly listed below we make no promises to support it.

| Format | Extra                   | Skimming | Thinning | Purpose               | Other Notes |
| ------ | ------------------------------- | -------- | -------- | --------------------- | ----------- |
| FTAG1  | :curly_loop: :black_circle: P J | None     | None     | Algorithm Development | MC Only     |
| FTAG2  | :curly_loop: :black_circle: P J | $e +\mu$ | Only GA tracks | Algorithm cross checks in data | Can also support ttbar based b-jet cablibration |
| FTAG3  | :curly_loop: :black_circle: P J | $j \| \mu$ | None | $g \to bb$ Xbb calibation | |
| FTAG4  | None | single lepton | `PHYS` | $b$, $c$ and light-jet calibations | |

Extra content key ("extra" is with respect to `PHYS`):

| Symbol | meaning |
| ------------ | ------ |
| :curly_loop: | tracks |
| :black_circle: | calorimeter clusters |
| P | pflow objects |
| J | All jet and b-jet information |


## Requesting Derivations

You normally don't need to produce derivations yourself.
Instead, follow the steps below to request derivation if you need one.

??? info "Justification for FTAG derivation dequests"

    You should only request derivations from the FTAG group for FTAG specific studies.
    Derivations for physics analysis should be requested centrally by the analysis contacts
    (not through FTAG), unless (exceptionally) the FTAG group also has some specific interest in the samples or analysis in question.

To request a derivation, follow the high-level steps below:

1. If you have a Merge Request (MR) you want to be included in the deriavtions, make sure it's merged and has been swept to main (often FTAG MRs target `24.0`, but the derivations run off `main`, so you need to wait for your MR to be swept to main). 
2. If the [latest Athena release](https://gitlab.cern.ch/atlas/athena/-/releases) doesn't include your MR, you will need to request a new release in the [ATLINFR JIRA](https://its.cern.ch/jira/browse/ATLINFR). See [here](https://its.cern.ch/jira/browse/ATLINFR-5410) for previous request that you can use as a template.
3. If/when a release is available, derivations can be requested via a JIRA ticket as described below: 
    - MC derivations: [ATLFTAGDPD](https://its.cern.ch/jira/projects/ATLFTAGDPD)  
    - Data derivations (or large collection of PHYS derivations on MC) : [ATLASDPD](https://its.cern.ch/jira/projects/ATLASDPD)


### Derivation requests on MC

Open a ticket under [ATLFTAGDPD](https://its.cern.ch/jira/projects/ATLFTAGDPD) including the information below.
The [FTAG derivation contacts](index.md#ftag-derivation-contacts) will then respond to your request.

??? info "Information to include"

    - Tag FTAG GROUP derivation contacts with JIRA tag handles `@sgoswami` & `@waislam`
    - Which samples you are requesting and why (e.g. ttbar and Z' samples for training).
    - Which derivation format (e.g. FTAG1).
    - Which release you would like the production in (e.g. `25.0.x`) and the corresponding p-tag.
    - A list of full 3-tag recon AOD names, split up by MC campaign
    - Note that merged/repeated tag samples with double or triple e/s/r tags aren't used for Derivation production (like `*r13145_r13144` etc.)
    - The list can be either in a collaborative CodiMD file or a plain text file uploaded to the JIRA or in the description itself.

??? abstract "Template FTAG1 Request on MC"

    Dear `<tag ftag derivation contacts>`

    We would like to request FTAG1 derivations for `<samples>` in release `<24.0.x>` (`<p-tag>`).

    The list of samples is below.

    MC23:
    ```
    mc23_13p6TeV.....
    mc23_13p6TeV.....
    mc23_13p6TeV.....
    ```

    Cheers,
    <name>

    tagging <interested parties>

!!! example "Example: [ATLFTAGDPD-377](https://its.cern.ch/jira/browse/ATLFTAGDPD-377)"


###  Derivation requests on data

Open a ticket under [ATLASDPD](https://its.cern.ch/jira/projects/ATLASDPD), including the information below.
The [central derivation contacts](index.md#central-data-derivation-contacts) will then respond to your request.

??? info "Information to include"

    - Tag all the Central derivation contacts ( JIRA tag handles of current CENTRAL derivation contacts are: `@emmat` `@fladias` & `@maklein` )
    - Data containers you are requesting the derivations on and why.
    - Which derivation format (e.g. FTAG1).
    - Which release you would like the production in (e.g. `25.0.x`) and the corresponding p-tag.
    - PHYS derivation requests on data containers need to be discussed with the Central Derivations Team before submission on their end.

??? abstract "Template PHYS Request on data"

    Dear `<tag central derivation contacts>`

    We would like to request PHYS derivations for `<data>` in release `<24.0.x>` (`<p-tag>`).

    The list is below.

    data15_13TeV.periodAllYear.physics_Main.PhysCont.AOD.repro26_v01

    Cheers,
    <name>

    tagging <interested parties>

!!! example "Example: [ATLASDPD-2101](https://its.cern.ch/jira/browse/ATLASDPD-2101)"


## Running Derivations Locally

Derivation in r22 (and above) are produced using the `Derivation_tf.py` command using the CA mode.

To begin, setup the release/nightly you would like the derivation to be launched.
```bash
setupATLAS
# only select one of the following options
asetup Athena,main,latest # if you want to test in the latest main 
asetup Athena,main,ryear-month-date # if you want to test a specific nightie, for example r2023-03-20
asetup Athena,25.0.5
```

To run a derivation routine locally, you'll need a local `AOD` sample. The most commonly used `AOD` files are listed in the [algorithm dataset overview](sample_list.md) page.

Next, run a derivation
```bash
    Derivation_tf.py --CA \
    --formats FTAG1 \
    --inputAODFile path/to/AOD.root \
    --maxEvents 100 \
    --outputDAODFile test.root \
```

If you want to use the config from an existing p-tag you can include `--AMIConfig pXXXX`.


??? warning "The main branch is a moving target and may be broken"
    Take a look [here](https://training-dataset-dumper.docs.cern.ch/tests/#making-daods-from-aods) for instructions on how to find a previously used release.

If you're interested in whether ART tests are successful with a given release, feel free to take a peek at [ART Tests FTAG](https://bigpanda.cern.ch/art/jobs/?branch=main/Athena/x86_64-el9-gcc13-opt&nlastnightlies=7&view=packages&package=DerivationFrameworkFlavourTagART) for `FTAG` derivations.

You can also make your own selections in [the ART mnenu selection page](https://bigpanda.cern.ch/art/) with a proper `athena` release and `gcc` version. 
A helpful pdf is also provided [here](https://twiki.cern.ch/twiki/pub/AtlasProtected/DerivationProductionTeam/art_athderivation.pdf)

??? info "Making test DAOD files for use with the TDD"
    There is also a guide to making test DAODs for use with the TDD [here](https://training-dataset-dumper.docs.cern.ch/tests/#making-inputs-for-tests).

??? info "Turning on a flag for special studies"
    Here is an example.
    ```
    --preExec 'flags.BTagging.Trackless=True'
    ```


### Making changes

Currently, all FTAG derivation related changes should target [main branch](https://gitlab.cern.ch/atlas/athena/-/tree/main) in athena.

```bash
setupATLAS
asetup Athena,main,latest
lsetup git
git atlas init-workdir ssh://git@gitlab.cern.ch:7999/atlas/athena.git -b main 
cd athena
git checkout -b main-my-test-branch upstream/main --no-track # to checkout a new branch for later making MRs
git push --set-upstream origin main-my-test-branch
git atlas addpkg DerivationFrameworkFlavourTag # or other packages you want
## make changes ##
cd ../ && mkdir build && cd build
cmake ../athena/Projects/WorkDir/
make 
source x*/setup.sh
## compile to do a local test, to make sure the change works ##
cd ../ && mkdir run && cd run
## then here in the run directory to perform a local test
```



## Tips / FAQ

#### AOD samples were deleted or are on TAPE

You would need to get hold of FTAG MC contacts and request the production of AODs if they don't exist.
This becomes much simpler if `HITS` files aren't deleted (and infinitely complicates the production if the `HITS` files are deleted from the grid).

If they are on `TAPE`, you would need to ask FTAG MC contacts to submit an `r2d2` request via [RUCIO Web UI](https://rucio-ui.cern.ch/) to restore the samples to some grid `CLOUD` storage element for rapid access.


#### Find all datasets for a sample

You can simply run (after changing mc20/mc23 and <6-digit-sample-id> to suit your needs):

```bash
setupATLAS -q
voms-proxy-init --voms atlas
lsetup rucio
lsetup panda
lsetup pyami

rucio ls mc20_13TeV.<6-digit-DSID>.*.recon.AOD.* | grep "CONTAINER" | grep -v "log"
rucio ls mc23_13p6TeV.<6-digit-RUN3-DSID>.*.recon.AOD.* | grep "CONTAINER" | grep -v "log"

```


#### I have raw sample names without e/s/r-tags

!!! info "More info on tags and samples names is available [here](index.md#tags)"

You can run the snippet below to get 3-tag `recon` AOD names, say, with `r tag` `r13145`
where `raw_samp_names.txt` has one raw sample name per line.

Snippets here may also be edited to make more general queries using `rucio`.

[ATLAS AMI](https://ami.in2p3.fr/) provides verbose info (production/campaign version) on your sample `e/s/r/p` tags.

```bash
setupATLAS -q
voms-proxy-init --voms atlas
lsetup rucio
for line in $(cat raw_samp_names.txt); do
        templist=$(rucio ls "$line"* | grep "recon" | grep -v "log" | grep "CONTAINER" | cut -c 2- | grep -E '.AOD\.e[0-9]{4}_[s][0-9]{4}_r[0-9]{5}.*' | rev | cut -c 22- | rev | grep -E "r13145" | sed 's/^[ \t]*//;s/[ \t]*$//')
        echo "$templist" >> fullsamplenames.txt
done
```


#### I want to check whether a derivation exists

Change mc20, <6-digit-sample-id>, FTAG1, 5770 to suit your needs.
Look for samples with each of single e,s,r & p tags

```bash
setupATLAS -q
voms-proxy-init --voms atlas
lsetup rucio
rucio ls mc20_13TeV.<6-digit-DSID>.*.deriv.DAOD_FTAG1.* | grep "CONTAINER" | grep -v "log" | grep "p5770"
```


#### I want to check that a derivation container is not empty

```bash
setupATLAS -q
voms-proxy-init --voms atlas
lsetup rucio
rucio list-files <full derivation dataset name queried above>
```
If the output is `0.0B` then, the container nominally exists but it isn't populated, in which case proceed with the derivation request by opening a new JIRA ticket.
