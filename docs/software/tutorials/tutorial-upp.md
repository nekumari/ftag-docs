# Preprocessing Tutorial

In this tutorial, we will learn how to use the standalone preprocessing package `umami-preprocessing`.

The code for the package is [here](https://github.com/umami-hep/umami-preprocessing), and documentation is available [here](https://umami-hep.github.io/umami-preprocessing//).

## Introduction

`umami-preprocessing` (UPP), is the preprocessing package used in FTAG to prepare
and process the jets coming from the Training-Dataset-Dumper (TDD). The datasets
(in .h5 format) from the dumper are prepared, separated in
training/validation/testing and resampled. The resampling is important to avoid a
kinematic bias in the training of the networks. It also ensures a smooth
transition between two samples. In case of FTAG, these two samples from which we
take the jets are $t\bar{t}$ and $Z'$. While $t\bar{t}$ jets are used for the low
$p_\text{T}$ region, a flat-mass $Z'$ sample, which decays into a pair of quarks,
$\tau$'s or electrons, is used to cover the high $p_\text{T}$ area up till 
$6\,\text{TeV}$.

In this tutorial, we will focus on the basics of how to run UPP and create a
training, validation and testing sample for the training of a $b$-tagger using
[Salt](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/salt/) (which is covered in [another tutorial](https://ftag-docs.docs.cern.ch/software/tutorials/tutorial-salt/)).

## Prerequisites
To work with UPP, you need to clone the repository to your workspace. This can
be done using the following command:

```bash
git clone https://github.com/umami-hep/umami-preprocessing.git
```

Now you can switch into the folder to find all configuration files and the source code.

### Install UPP

First you need to install up so that you can follow the tutorial.
Follow the instructions in the `umami-preprocessing` [documentation](https://umami-hep.github.io/umami-preprocessing/setup/) to install the package.

The simplest possible way is to

```bash
pip install umami-preprocessing
```

If you want (and you should want to consider this), you could also set up a virtual environment or a conda/mamba environment before the installation via `pip`.


### Input Datasets
The input datasets we want to use are stored on `lxplus`. You can either copy
them directly (if you are on lxplus) or download them via `wget`. You can copy
them using the following command:

```bash
mkdir samples
cp /eos/user/u/umamibot/www/ci/tutorial/upp/*.h5 samples/
```

If you need to download them:

```bash
mkdir samples
cd samples
wget https://umami-ci-provider.web.cern.ch/tutorial/upp/ttbar.h5
wget https://umami-ci-provider.web.cern.ch/tutorial/upp/zpext.h5
```

Once all the samples are downloaded, you can continue with the tutorial tasks.

## Tutorial tasks
The main task of this tutorial, as already-mentioned, is to create a little
training dataset which can be used for the training of a $b$-tagging algorithm.
To make this task easier and more understandable, we split the full process into
multiple sub-tasks with tips and solutions, so you don't get stuck.

### 1. Optional: Plot Input Distributions
A good practice is to first plot the input variables before you blindly plot them. You can write plotting scripts using puma for doing so, although there are many possible ways to plot the input distributions of the variables.

Try to plot the jet $p_\text{T}$ and $\eta$ distributions of the two samples. You can find the puma plotting tutorial [here](https://ftag.docs.cern.ch/software/tutorials/tutorial-plotting/). You can also skip this step if you only want to learn about the umami preprocessing workflow.

Note that you should have installed also [`puma`](https://github.com/umami-hep/puma) for convenient plot creation. You can do so with

```bash
pip install puma-hep
```

??? warning "Solution: Plot Input Distributions"

    You can plot for example the $p_\text{T}$ distribution with the following snippet.
    Note that you have to replace `<path_to_ttbar_h5_file>` with the actual path to the ttbar file you downloaded.

    ```
    import h5py
    from puma import Histogram, HistogramPlot

    ttbar_filepath = "<path_to_ttbar_h5_file>"

    # load the jets dataset from the h5 file
    with h5py.File(ttbar_filepath, "r") as h5file:
        jets = h5file["jets"][:]

    # defining boolean arrays to select the different flavour classes
    is_light = jets["HadronConeExclTruthLabelID"] == 0
    is_c = jets["HadronConeExclTruthLabelID"] == 4
    is_b = jets["HadronConeExclTruthLabelID"] == 5

    # initialise the plot
    pt_plot = HistogramPlot(
        bins_range=(0, 250_000),
        xlabel="$p_T$ [MeV]",
        ylabel="Normalised number of jets",
    )

    # add the histograms
    pt_plot.add(Histogram(jets[is_light]["pt_btagJes"], flavour="ujets"))
    pt_plot.add(Histogram(jets[is_c]["pt_btagJes"], flavour="cjets"))
    pt_plot.add(Histogram(jets[is_b]["pt_btagJes"], flavour="bjets"))

    pt_plot.draw()
    pt_plot.savefig("tutorial_histogram_pT.png")
    ```


### 2. Modify the UPP configuration files
After we created the now the plots of the input distributions of the variables, we
want to resample. For that, we can use the default config of `UPP`. This can be 
found in the `umami-preprocessing` in `upp/configs/single-b.yaml`. Your first
task will be to adapt the config to work for our small test samples we are going
to use in this tutorial. \\

Familiarize yourself with the config using the documentation and change the
following.

#### 2.1 Modify the Global Variables
The first step is to adapt the global variables correctly. Adapt the `ntuple_dir`
and `base_dir` to your paths and set the `batch_size` to 10k. Due to the small
size of our test samples here, set the `num_jets_estimate` to 400k.

??? info "Hint: Modify the Global Variables"

    A detailed explanation of all the global variables can be found
    [here](https://umami-hep.github.io/umami-preprocessing//configuration/#global-config)

??? warning "Solution: Modify the Global Variables"

    `base_dir` is the path were all the outputs will be saved. Take an empty
    folder. `ntuple_dir` is the path to the folder were your samples from
    the dumper (.h5) are stored. The other two should be simple to change.

#### 2.2 Modify the Input Samples
The next step involves changing the input samples. Adapt the two input sample blocks
for the $t\bar{t}$ and $Z'$ accordingly to our two used samples.

??? info "Hint: Modify the Input Samples"

    A detailed explanation of the input samples block can be found
    [here](https://umami-hep.github.io/umami-preprocessing/configuration/#input-h5-samples)

??? warning "Solution: Modify the Input Samples"

    The input sample blocks should look like this:

    ```yaml
    ttbar: &ttbar
        name: ttbar
        pattern:
            - "ttbar*.h5"

    zprime: &zprime
        name: zprime
        pattern:
            - "zpext*.h5"
    ```

#### 2.3 Modify the Resampling Regions
Due to our low statistics example samples, we need to change a bit the kinematic regions
for the two samples. Change the resampling regions accordingly: Low $p_\text{T}$ should
be $20\,\text{GeV}$-$150\,\text{GeV}$ and high $p_\text{T}$ from 
$150\,\text{GeV}$-$6\,\text{TeV}$.

??? info "Hint: Modify the Resampling Regions"

    A detailed explanation of the resampling regions can be found
    [here](https://umami-hep.github.io/umami-preprocessing/configuration/#resampling-regions)

??? warning "Solution: Modify the Resampling Regions"

    The resampling region blocks should look like this:

    ```yaml
    lowpt: &lowpt
        name: lowpt
        cuts:
            - [pt_btagJes, ">", 20_000]
            - [pt_btagJes, "<", 150_000]
    highpt: &highpt
        name: highpt
        cuts:
            - [pt_btagJes, ">", 150_000]
            - [pt_btagJes, "<", 6_000_000]
    ```

#### 2.4 Modify the Components
As already mentioned, we are working with small size samples here. Therefore, we
need to adapt also the number of jets we want to have in the end for each flavour.
Change the numbers to 100k jets for $b$- and $c$-jets and 200k for light-flavour
jets for $t\bar{t}$ and 5k jets for $b$- and $c$-jets and 10k for light-flavour
jets for $Z'$. In addition, we want to remove the $\tau$ jets for now, due to
their very low statistics in our small size samples.

??? info "Hint: Modify the Components"

    A detailed explanation of the components block can be found
    [here](https://umami-hep.github.io/umami-preprocessing//configuration/#components)

??? warning "Solution: Modify the Components"

    The components block should look like this:

    ```yaml
    components:
        - region:
            <<: *lowpt
        sample:
            <<: *ttbar
        flavours: [bjets, cjets]
        num_jets: 100_000

        - region:
            <<: *lowpt
        sample:
            <<: *ttbar
        flavours: [ujets]
        num_jets: 200_000

        - region:
            <<: *highpt
        sample:
            <<: *zprime
        flavours: [bjets, cjets]
        num_jets: 5_000

        - region:
            <<: *highpt
        sample:
            <<: *zprime
        flavours: [ujets]
        num_jets: 10_000
    ```

#### 2.5 Modify the Resampling Options
We want to use the `pdf` resampling method with the `bjets` as target 
distribution. The sampling fraction can stay at `auto`, but you can also play around
with it a bit.

??? info "Hint: Modify the Resampling Options"

    A detailed explanation of the resampling options block can be found
    [here](https://umami-hep.github.io/umami-preprocessing//configuration/#resampling)

??? warning "Solution: Modify the Resampling Options"

    The resampling options blocks should look like this:

    ```yaml
    resampling:
        target: bjets
        method: pdf
        sampling_fraction: auto
        variables:
            pt_btagJes:
                bins: [[20_000, 250_000, 50], [250_000, 1_000_000, 50], [1_000_000, 6_000_000, 50]]
            absEta_btagJes:
                bins: [[0, 2.5, 20]]
    ```

#### 2.6 Modify the Variables
After the main preprocessing config is adapted, we still need to look into the 
variables. These are added at the top of config file and are out-sourced to
another file. From the already-existing `variables.yaml` we want to keep the 
following jet labels: `HadronConeExclTruthLabelID`, `HadronConeExclTruthLabelPt`,
`HadronConeExclExtendedTruthLabelID`, `pt`, `eta`, `mass`. From the track labels,
we want to keep only the `truthOriginLabel`. In addition, the name of the tracks
in our samples is different. Try to figure out the correct name using `h5ls` and
adapt it accordingly.

??? info "Hint: Modify the Variables"

    A detailed explanation of the resampling options block can be found
    [here](https://umami-hep.github.io/umami-preprocessing//configuration/#variables)

    Also, using the `h5ls` shows you the name of the datasets in the .h5 file. The
    first one is `jets` the second one is the name of the tracks dataset.

??? warning "Solution: Modify the Variables"

    The variables should look like this:

    ```yaml
    jets:
        inputs:
            - pt_btagJes
            - eta_btagJes
        labels:
            - HadronConeExclTruthLabelID
            - HadronConeExclTruthLabelPt
            - HadronConeExclExtendedTruthLabelID
            - pt
            - eta
            - mass

    tracks_loose:
        inputs:
            - d0
            - z0SinTheta
            - dphi
            - deta
            - qOverP
            - IP3D_signed_d0_significance
            - IP3D_signed_z0_significance
            - phiUncertainty
            - thetaUncertainty
            - qOverPUncertainty
            - numberOfPixelHits
            - numberOfSCTHits
            - numberOfInnermostPixelLayerHits
            - numberOfNextToInnermostPixelLayerHits
            - numberOfInnermostPixelLayerSharedHits
            - numberOfInnermostPixelLayerSplitHits
            - numberOfPixelSharedHits
            - numberOfPixelSplitHits
            - numberOfSCTSharedHits
            - leptonID
        labels:
            - truthOriginLabel
    ```

### 3. Run Train Preprocessing
After playing around now with the config and adapt it for our needs, we can start
the procedure of the resampling. The preprocessing runs by default completely 
which we don't want to do here. We will do this step-by-step here using the
respective commands for that.

#### 3.1 Prepare Step
The first step of the preprocessing is the preparation of the histograms from
which we extract the needed information for the resampling. Switch to `umami-preprocessing` directory and run the first step of the preprocessing.

??? info "Hint: Prepare Step"

    A detailed explanation of the running procedure can be found
    [here](https://umami-hep.github.io/umami-preprocessing/run/)

??? warning "Solution: Prepare Step"

    The command should look like this:

    ```bash
    preprocess --config upp/configs/single-b.yaml --prep
    ```
#### 3.2 Resampling Step
The second step we need to run is the resampling. Here, the actual resampling
takes place where the different flavours are resampled in the defined resampling
variables/bins. This will be done by the program in parallel for all 
flavours/samples. \\
Try to run now the resampling step!

??? info "Hint: Resampling Step"

    A detailed explanation of the running procedure can be found
    [here](https://umami-hep.github.io/umami-preprocessing/run/)

??? warning "Solution: Resampling Step"

    The command should look like this:

    ```bash
    preprocess --config upp/configs/single-b.yaml --resample
    ```

    We can see that the sampling fraction is above one for some of the flavours.
    Using the `auto` method sets this value to the minimal possible one, which
    is show in the warning.

#### 3.3 Merge and Normalise Step
The last two steps which are needed to fully create the training dataset is the
merging and normalising. Here, the resampled per flavour/sample samples are 
merged and shuffled and then the normalisation is calculated and saved.
(default: Setting the mean/std for all variables to 0/1). \\
Try to run this now!

??? info "Hint: Merge and Normalise Step"

    A detailed explanation of the running procedure can be found
    [here](https://umami-hep.github.io/umami-preprocessing/run/)

??? warning "Solution: Merge and Normalise Step"

    The command should look like this:

    ```bash
    preprocess --config upp/configs/single-b.yaml --merge
    preprocess --config upp/configs/single-b.yaml --norm
    ```
### 3.4 Plotting Step
Now that we have created a training dataset, it is also nice to verify that the
resampling did what it should and nothing crazy. This can be done by plotting
the resampled distributions of the resampling variables. \\
Try to do this and verify everything looks like it should!

??? info "Hint: Plotting Step"

    A detailed explanation of the running procedure can be found
    [here](https://umami-hep.github.io/umami-preprocessing/run/)

??? warning "Solution: Plotting Step"

    The command should look like this:

    ```bash
    preprocess --config upp/configs/single-b.yaml --plot
    ```

    You will get a plot for the low and high $p_\text{T}$ area as well as a
    $|\eta|$ plot. They should look similar to these:

    ![Low $p_\text{T}$](assets/train_pt_btagJeslow.png)
    ![High $p_\text{T}$](assets/train_pt_btagJes.png)
    ![$|\eta|$](assets/train_absEta_btagJes.png)

### 4. Optional: Run also Validation and Testing
In addition to the training dataset, we also require a validation and a testing
sample to validate (while training) and test (after training) the performance of
the network. Try to re-run the steps in Section 4 for both the validation and
testing datasets!

??? info "Hint: Run also Validation and Testing"

    A detailed explanation of the running procedure and the splits can be found
    [here](https://umami-hep.github.io/umami-preprocessing//run/#splits)

??? warning "Solution: Run also Validation and Testing"

    You can simply run the command without the step definition but with the split
    argument

    ```bash
    preprocess --config upp/configs/single-b.yaml --split=all
    ```