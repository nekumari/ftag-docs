# FTAG in Athena

B-tagging runs at both Reconstruction and Derivation levels, but by default, the outputs are only saved in Derivation. the following text summarizes both workflows. The overall structure of the workflow is illustrated below.

![Overview of FTAG software workflow](assets/AthenaBTaggingDiagram.png)

## Create a MR in athena

Details of how to create a MR to athena can be found in the [ATLAS software documentation](https://atlassoftwaredocs.web.cern.ch/gittutorial/env-setup/).

In short, a few steps are needed:

- Make a fork of the central athena repository and add `ATLAS Robot` as developer to your fork [as documented here](https://atlassoftwaredocs.web.cern.ch/gittutorial/gitlab-fork/)

- create a new branch in your fork

```
setupATLAS
lsetup git
# assuming you want to make a MR to the main branch
git atlas init-workdir ssh://git@gitlab.cern.ch:7999/atlas/athena.git -b main
cd athena
git fetch upstream
git checkout -b you-branch-name upstrea/main --no-track
git push --set-upstream origin you-branch-name
```

- Checkout the packages you want to modify

```
# still under athena folder
git atlas addpkg package-in-athena
# then make modification
# if you want to test it locall, you can do it by:
# cd ../; mkdir build; cd build; cmake ../athena/Project/Workdir/; make; source x*/setup.sh
# git add, commit and push
```

- Make a Merge Request as described [here](https://atlassoftwaredocs.web.cern.ch/gittutorial/merge-request/)


## Athena MR Policy

The branching of `24.0` from `main` is completed in March, 2024. Since then, a guideline has been released by the core software team for submitting merge requests either to `24.0` or `main`.
![Overview of general software policy](assets/branching_guidelines.png)

**However, FTAG MR policy does not fully align with the general guidline. Please check the following for details:**

- If your MR changes the following packages, please target `24.0`:
  - ML code in [FlavorTagDiscriminants](https://gitlab.cern.ch/atlas/athena/-/tree/main/PhysicsAnalysis/JetTagging/FlavorTagDiscriminants)
  - Validation code in [JetTagDQA](https://gitlab.cern.ch/atlas/athena/-/tree/main/PhysicsAnalysis/JetTagging/JetTagValidation/JetTagDQA)

### FT0 and frozen derivation policy

Reconstruction is run from `24.0` branch which is operating under Frozen Tier 0 policay (i.e. in which changes to job output in AOD files are tightly controlled). In the case of FT0 violation, please reach out to the FTAG software conveners. It will need to be discussed at the Reconstruction meeting for approval.

As BTagging and JetEtMiss collections are not saved in AOD by default, those branches are not protected by the Frozen Tier 0 (TF0) policy. To protect those branches, a frozen derivaiton CI test on PHYS was added in July 2023 by FTAG group. Derivation in run from `main` branch which is operating under frozen derivaiton policy. **In the case of frozen derivation violation related to b-tagging variables, an approval from FTAG software conveners are required.**

For both policy, once it's approved, the reference files will be updated by the RC on shift that week.


