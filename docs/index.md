# ATLAS Flavour Tagging Documentation

Welcome to the ATLAS Flavour-Tagging group page.
This replaces the old [FTAG Twiki](https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/FlavourTagging?noredirect=on).

!!!question "Doing physics? [Click here for recommendations!](recommendations/index.md)"

???bug "Questions or bug reports?"

    [Bugs should be reported on JIRA](https://its.cern.ch/jira/projects/AFT). If you have any other questions please [ask on atlas-talk][atalk].

The FTAG group is split into four subgroups:

- The [algorithms subgroup](algorithms/index.md) develops and maintains the heavy flavour tagging algorithms.
- The [calibrations subgroup](calibrations/index.md) measures the efficiency of the algorithms in data and simulated MC events and provides corrections to the simulated events for use in analysis.
- The [software subgroup](software/index.md) maintains the software underlying these efforts.
- The [Xbb subgroup](https://xbb-docs.docs.cern.ch) studies algorithms and their calibration for identifying large-radius jets, such as Higgs-jets decaying to b- and c-quarks.

This website contains [group-level recommendations](recommendations/index.md) and subgroup-specific documentation in the different tabs at the top of the page (with the exception of Xbb which is hosted on a separate website). 



## General Information

### Conveners

- [Valentina Vecchio](https://atlas-glance.cern.ch/atlas/membership/members/profile?id=11482)
- [Dan Guest](https://atlas-glance.cern.ch/atlas/membership/members/profile?id=5921)

to be reached via <atlas-perf-flavtag-conveners@cern.ch>

### Staying informed

You can stay up to date with the flavor tagging subgroups by:

- Following discussion in [the flavor tagging category in atlas-talk][atalk]. You should ask questions here first.
- Subscribing to egroups via the [CERN egroups webpage](https://e-groups.cern.ch/e-groups/EgroupsSubscription.do)
- Joining mattermost groups (see below)
- Subscribing to calendars via indico.

[atalk]: https://atlas-talk.web.cern.ch/c/ftag

??? Tip "Subscribe to calendars"

    We've split the [top level official meetings](https://indico.cern.ch/category/9120/) into subcategories. You should be able to subscribe to any of the subgroup meetings by copying the subscribe link to your local calendar program.
    ![do it like this!](assets/subscribe.png)

| Subgroup     | e-group                                | Email                                                  |
|:------------:|:--------------------------------------:|:-------------------------------------------------------|
| Plenary      | [\[join\]][em-pj] [\[archive\]][em-pa] | <hn-atlas-flavourTagPerformance@cern.ch>               |
| Algorithms   | [\[join\]][em-aj] [\[archive\]][em-aa] | <atlas-cp-flavtag-btagging-algorithms@cern.ch>         |
| Calibrations | [\[join\]][em-cj] [\[archive\]][em-ca] | <atlas-cp-flavtag-calibrations@cern.ch>                |
| Software     | [\[join\]][em-sj] [\[archive\]][em-sa] | <atlas-cp-flavtag-software@cern.ch>                    |
| Xbb          | [\[join\]][em-xj] [\[archive\]][em-xa] | <atlas-cp-flavtag-jetetmiss-BoostedXbbTagging@cern.ch> |


[em-pj]: https://e-groups.cern.ch/e-groups/EgroupsSubscription.do?egroupName=hn-atlas-flavourTagPerformance
[em-pa]: https://groups.cern.ch/group/hn-atlas-flavourTagPerformance
[em-aj]: https://e-groups.cern.ch/e-groups/EgroupsSubscription.do?egroupName=atlas-cp-flavtag-btagging-algorithms
[em-aa]: https://groups.cern.ch/group/atlas-cp-flavtag-btagging-algorithms
[em-cj]: https://e-groups.cern.ch/e-groups/EgroupsSubscription.do?egroupName=atlas-cp-flavtag-calibrations
[em-ca]: https://groups.cern.ch/group/atlas-cp-flavtag-calibrations
[em-sj]: https://e-groups.cern.ch/e-groups/EgroupsSubscription.do?egroupName=atlas-cp-flavtag-software
[em-sa]: https://groups.cern.ch/group/atlas-cp-flavtag-software
[em-xj]: https://e-groups.cern.ch/e-groups/EgroupsSubscription.do?egroupName=atlas-cp-flavtag-jetetmiss-BoostedXbbTagging
[em-xa]: https://groups.cern.ch/group/atlas-cp-flavtag-jetetmiss-BoostedXbbTagging


### Meetings

FTAG Plenary meetings take place every Tuesday at 15h30 CERN time [[indico]](https://indico.cern.ch/category/9120/).

Overviews of all pleneary and subgroup meetings can be found in the left side-bar.


### Mattermost
You can join our Mattermost teams here:

[Algorithms + Software](https://mattermost.web.cern.ch/signup_user_complete/?id=1wicts5csjd49kg7uymwwt9aho&md=link&sbr=su){ .md-button .md-button--primary }
[Calibrations](https://mattermost.web.cern.ch/signup_user_complete/?id=9xp856ytcj87tqwfjs3g17rywc&md=link&sbr=su){ .md-button .md-button--primary }
[Xbb](https://mattermost.web.cern.ch/signup_user_complete/?id=fdodok94p3nhmxmkipo7z9uxkc&md=link&sbr=su){ .md-button .md-button--primary }

They are used to discuss a variety of topics and is the easiest way to ask questions.


### JIRA

The main ATLAS Flavour Tagging (AFT) Jira project can be found [here](https://its.cern.ch/jira/projects/AFT/).
An overview of open issues and AQPs in the group can be found at [this JIRA dashboard](https://its.cern.ch/jira/secure/Dashboard.jspa?selectPageId=22701).


### Contacts

Note that we're seeking replacements for some of these positions, please consider volunteering. We'll give you Operational Task Planning (OTP) credit in proportion to the time you spend on the task.

#### FTAG Derivation contacts

- Soumyananda Goswami <soumyananda.goswami@cern.ch>
- Wasikul Islam <wasikul.islam@cern.ch>

See [here](samples/daod.md) for details on requesting FTAG DAODs.

#### Central Data Derivation contacts

- Flavia De Almeida Dias <flavia.dias@cern.ch>
- Matthew Henry Klein <matthew.henry.klein@cern.ch>

See [here](samples/daod.md) for details on requesting non-FTAG DAODs.

#### MC and PMG

- Yi Yu <yi.yu@cern.ch>
- Qibin Liu <qibin.liu@cern.ch>

See [here](samples/mc_requests.md) for details on requesting AODs.

#### AMG

- Walter Leinonen <waltteri.leinonen@cern.ch>

#### Validation

##### Physics Validation

- (Burhani) Taher Saifuddin <burhani.taher.saifuddin@ou.edu>
- Xuan Yang <xuan.yang@cern.ch>

##### Data Quality

Data quality [documentation is hosted on a designated page][dq].

??? Tip "We give OTP for shifts! Want to sign up?"

    We give class-1 OTP for shifts in the Atlas Control Room (ACR) or class-2 OTP for offline shifts, which can be done remotely.

    You can sign up for these by emailing the conveners listed in [the DQ page][dq].

[dq]: https://ftag-dq.docs.cern.ch/

#### Disk Space Manager

- Leonardo Toffolin <leonardo.toffolin@cern.ch>

#### Calibration Data Interface (CDI) Maintainer

- Zhuoran Feng <z.feng@nikhef.nl>
- **Help Wanted:** we are looking for a second maintainer to fill in for Zhuoran while she finishes her PhD!

#### Social Chair

- [Ivo Young](https://atlas-glance.cern.ch/atlas/membership/members/profile?id=18486): @ivyoung or <i.young.1@research.gla.ac.uk>

## Contributing

The documentation source is hosted on GitLab in the 
[ftag-docs](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/ftag-docs/) 
repository.

[ccd]: https://how-to.docs.cern.ch
[ccd-mkdocs]: https://how-to.docs.cern.ch/gitlabpagessite/create_site/creategitlabrepo/create_with_mkdocs/

We encourage anyone to contribute to these documentation pages by opening a merge request there.
To serve the docs locally, you can clone the repo and use `mkdocs` see your changes.

```bash
git clone ssh://git@gitlab.cern.ch:7999/atlas-flavor-tagging-tools/algorithms/ftag-docs.git
cd ftag-docs
python -m pip install -r requirements
mkdocs serve
```

You will see a link to open a locally hosted version of the documentation pages in a browser.
These will auto-update with changes to the source files.

## Forking this Page

To host your own version of this page at CERN, see CERN's [how-to.docs.cern.ch][ccd], specifically [the page on how to set up a MkDocs instance][ccd-mkdocs].

