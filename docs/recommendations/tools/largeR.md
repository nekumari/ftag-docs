# BTagging Tools for Large-R Jet Tagging

The calibration results of large-R tagging is stored in JSON files. Same as small-R tagging, a few tools in [xAODBTaggingEfficiency](https://gitlab.cern.ch/atlas/athena/-/tree/main/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/xAODBTaggingEfficiency) are provided to read and apply the calibration results:

- `BTaggingSelectionJsonTool`: this interface allows analysers to calculate the tagging discriminant and decision for a specific working point on a large-R jet.
- `BTaggingEfficiencyJsonTool`: this interface allows analysers to calculate the tagging SFs and uncertianties on a jet.

An example of how to use these tools can be found in [BTaggingJsonToolsTester](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/xAODBTaggingEfficiency/util/BTaggingJsonToolsTester.cxx).
