# BTagging Tools for Small-R Jet Tagging

The calibration results of small-R b-tagging is stored in CDI files. A few tools exist in [xAODBTaggingEfficiency](https://gitlab.cern.ch/atlas/athena/-/tree/main/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/xAODBTaggingEfficiency) are available to access the calibration results and apply the calibraiton:

- `BTaggingSelectionTool`: this interface allows analysers to calculate the tagging discriminant and the b-tagging decision on a jet. 
- `BTaggingEfficiencyTool`: this interface allows analysers to get the b-tagging SFs and its uncertainties.

A unit test is provided in [BTaggingToolsTester](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/xAODBTaggingEfficiency/util/BTaggingToolsTester.cxx) as an example to show how to use these tools in analyses.

For more information on what these tools do, see [the CDI documentation under the calibration docs](../../calibrations/cdi/index.md).
