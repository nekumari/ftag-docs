# Flavor Tagging Recommendations (r22)

!!! Tip "looking for release 21?"
    The release 21 recommendations are [summarized on an older twiki][tw]

[tw]: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTagCalibrationRecommendationsRelease21

This page summarizes the supported release 22/25 FTAG recommendations for physics analysis.
As a physics analyst you might be also interested in:

1. [Expected algorithm performance in MC](algs/r22-preliminary.md).
2. [FTAG Tools for analysis](tools/smallR.md)

??? danger "Removed: VR Jets and Fixed Cut Working Points"

    In an effort to streamline our development and avoid confusion in analysis teams, we've dropped several recommendations in 22/25

    - The GN2v01 CDI file only provides "pseudocontinuous" working points. The "fixed cut" working points have been removed.
    - Variable Radius (VR) Track jets are no longer $b$-tagged.

!!! danger "2024 data is not yet supported"


## Recommended Tagger: GN2v01

The current recommended tagger is GN2v01. Details about the GN2v01 tagger optimization and the working point definition can be found [in the algorithm documentation](https://ftag.docs.cern.ch/recommendations/algs/r22-preliminary/#recommendation-as-of-07032024).

Two Calibration Data Interface (CDI) files, containing data-to-simulation scale factors for GN2v01 are available for physics analyses. They are summarized below.

| Data Period       | MC Campaign | CDI File name ([see below](#where-to-get-cdi-files) for full path) |
|-------------------|-------------|--------------------------------------------------------------------|
| Run 2             | MC20        | `13TeV/MC20_2024-10-17_GN2v01_v1.root`                             |
| Run 3 (2022-2023) | MC23        | `13p6TeV/MC23_2024-10-17_GN2v01_v1.root`                           |

Be aware of the following caveats:

- You must use analysis release **25.2.31 or later.**
- There is no support for MC21.
- You must use p-tag `p5980` or later for `PHYS` (`p6062` for `PHYSLITE`).
- The supported jet collection is `AntiKt4EMPFlowJets`.
- Only pseudocontinuous $b$-tagging (PCBT) scale factors are supported. The fixed cut working points have been removed. The PCBT scale factors do not apply the "smoothing" which was applied as a function of jet $p_{\rm T}$ for the fixed cut case.


Validation plots of the CDI Files content can be found [in a slide deck from the flavor tagging calibration meeting](https://indico.cern.ch/event/1463069/#2-cdi-approval).

Calibrations are provided for b-jets, charm-jets and light-jets while recommendations for tau-jets are extrapolated from the charm-jets calibration.

The pseudo-continuous scale factors correspond to five cut values. These select 90%, 85%, 77%, 70% and 65% of b-jets in a $t\bar{t}$ reference sample.

High pt extrapolation uncertainties (up to 3 TeV) are provided for all flavours and for both MC20 and MC23.

### MC-to-MC corrections

MC-to-MC correction factors are provided for both Run2 and Run3, the generators and DSIDs are summarised in the table below.

| MC campaign | Reference Generator (DSID) | Alternative Generators (DSID) |
| ------ | ---- | ---- |
| MC20    | PowhegPythia8EvtGen (410470) | PowhegHerwig7 (411233), PhH7EG (600666), Sherpa 2.2.12 (700660), Pythia8EvtGen517 (410480) |
| MC23a/d | PowhegPythia8EvtGen (601229) | PhH7EG (601414), Sherpa 2.2.14 (700808), Pythia8EvtGen517 (601398) |

!!! info "MC-to-MC maps for versions of Sherpa between 2.2.11 and 2.2.16 are equivalent."
    The FTAG code correctly accounts for this already: maps from Sherpa 2.2.12 in MC20 are automatically applied to Sherpa 2.2.14. See [!73443](https://gitlab.cern.ch/atlas/athena/-/merge_requests/73443) for details.


!!! question "Is your analysis sensitive to charm and/or light-jets FTAG uncertainties?"
    The current SFs for charm and light-jets have an uncertainty which is inflated due to 
    lack of MC23 statistics. If your analysis is affected by this, please get in contact with us. 

### Technicalities for GN2v01
A new output `tau` class has been added to the traditional `b`, `c` and `u` clases. For analyses, in order to use the correct GN2v01 tagger score, please update the [getTaggerWeight](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/xAODBTaggingEfficiency/xAODBTaggingEfficiency/BTaggingSelectionTool.h?ref_type=heads#L57) and [accept](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/xAODBTaggingEfficiency/xAODBTaggingEfficiency/BTaggingSelectionTool.h?ref_type=heads#L44) to include the `ptau` score when calling from [BTaggingSelectionTool](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/xAODBTaggingEfficiency/xAODBTaggingEfficiency/BTaggingSelectionTool.h?ref_type=heads). The minimum AthAnalysis (or AnalysisBase) release to get `ptau` correctly accounted for must be 25.2.2[^ptaufoot]. 

[^ptaufoot]: The `ptau` variable is technically supported in release 25.2.1 as well, but using it there is error prone, thus we recommend 25.2.2 unless you understand the risks that [were mitigated in 25.2.2][ptaubinbin].

[ptaubinbin]: https://gitlab.cern.ch/atlas/athena/-/merge_requests/69344

## Superseded recommendation (DL1dv01 b-tagging)

!!! bug "Release 25.2.30 or higher is required for PCBT"

    Due to a bug in the `BTaggingEfficiencyTool` releases between 25.2.15 and 25.2.29 will give incorrect pseudocontinuous scale factors.

The obsolete release 24 tagger is `DL1dv01`. See [PUB](https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PUBNOTES/ATL-PHYS-PUB-2020-014/) and [Plots](http://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PLOTS/FTAG-2021-004/) for comparison to `DL1r`.
This tagger can be used only by physics analyses not sensitive flavour tagging that already started the analysis development using `DL1dv01`.

The two CDI Files contain recommendations only forthe `AntiKt4EMPFlowJets` jet collection. 
Scale factors are provided for both FixedCut and PCBT working points.

| Tagger | Use Case | CDI Status | Limitations | p-tag |
| ------ | -------- | ---------- | ------- | ------------ |
| (superseded) DL1dv01 | Run 2 + MC20 | [done][dl1d20][^dl1d20] | No extrapolation uncertainties for PCBT | p5226 |
| (superseded) DL1dv01 | Run 3 + MC21 | [done][dl1d23][^dl1d23] | Can be used for jets with pT < 400 GeV | p5226 |

Notes:

- High pt extrapolation uncertainties are _only_ provided for Run 2 DL1dv01, using the fixed efficiency (non-PCBT) working points.

[dl1d20]: https://atlas-groupdata.web.cern.ch/atlas-groupdata/xAODBTaggingEfficiency/13TeV/2023-22-13TeV-MC20-CDI-2023-09-13_v1.root
[dl1d23]: https://atlas-groupdata.web.cern.ch/atlas-groupdata/xAODBTaggingEfficiency/13p6TeV/2023-22-13TeV-MC21-CDI-2023-09-13_v1.root
[^dl1d20]: File: `13TeV/2023-22-13TeV-MC20-CDI-2023-09-13_v1.root`
[^dl1d23]: File: `13p6TeV/2023-22-13TeV-MC21-CDI-2023-09-13_v1.root`


## Where to get CDI files

All Calibration Data Interface (CDI) Files the files are accessible via `eos`, `https`, and `cvmfs`. The following base paths are equivalent:

| system | path |
| ------ | ---- |
| cvmfs | `/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/xAODBTaggingEfficiency` |
| eos | `/eos/atlas/atlascerngroupdisk/asg-calib/xAODBTaggingEfficiency` |
| https | [`https://atlas-groupdata.web.cern.ch/atlas-groupdata/xAODBTaggingEfficiency/`](https://atlas-groupdata.web.cern.ch/atlas-groupdata/xAODBTaggingEfficiency) |

A full path will be of the form `<file-system>/<cdi-version>`, i.e.

```
/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/xAODBTaggingEfficiency/13TeV/<mc>_<date>_<tagger>_<version>.root
```

