# Soft Electron tagger

## DL1re (R22, ttbarOnly, last update 7/29/2022)

Since 20%(10%) of $b(c)$-hadrons have electrons in their decay, the previous/current recommended flavour tagging algorithm, DL1r/d, might benefit from electrons from heavy flavor decays. The current `DL1re` tagger includes 29 electron variables for training. The electron with the highest `phf` value, the probability of being an electron from the heavy flavour decays, is selected as the soft electron candidate inside the jets. `phf` cuts for the soft electron candidates are not recommended. The soft electron tagger is being summarised in a [note (very preliminary draft)](https://cernbox.cern.ch/index.php/apps/files/?dir=/workdir/softe/doc&#pdfviewer). Check the document for a detailed discussion of all the variables added. The soft electron presentations in the ftag meetings are summarised below.

This document summarises how to add the 29 soft electrons to DL1r and check the improved background jet rejections in practice. The process can be divided to two steps: 
- [First, include electron variables in the training-dataset-dumper](#TDD with electrons)
- [Second, include electron variables in the Umami training](#Umami with electrons)

In addition, the last section summarises some tips for future development of the soft electron tagger: 
- [Advanced: soft electron tagging development](#advanced)
    - [soft electrons in training-dataset-dumper](#advanced-tdd)
    - [soft electrons in Umami](#advanced-umami)

### training-dataset-dumper with electrons<a name="TDD with electrons"></a>
The current modification for SET is based on the [official r22 training-dataset-dumper (TDD) framework](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper), whose documentation explains in details how to install and run the dumper to create training samples from DAODs. This document focuses on the changes needed to soft electrons. 

To add the electron variables to the training samples, setup up the Athena environment instead of the Analaysisbase environment:
```bash
git clone ssh://git@gitlab.cern.ch:7999/atlas-flavor-tagging-tools/training-dataset-dumper.git
source training-dataset-dumper/setup-athena.sh
mkdir build
cd build
cmake ../training-dataset-dumper
make
cd ..
source build/x*/setup.sh
```
The current electron augmenter uses GaudiKernel from Athena for calculating electron ID DNN scores as the DNN scores are not stored in the default PHYSVAL DAOD for flavor tagging. Check the [electron augmenter MR1](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/merge_requests/396) for the discussion. 

After compiling, to add electron variables, set:  
```json
        "hfe": true
```
See [`configs/single-b-tag/EMPFlowSlim_softe.json(TBU)`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/blob/89802b14772da7ccf423efc5a98d183754be96ae/configs/single-b-tag/EMPFlowSlim_softe.json#L62) for an example. 

Make sure all 29 electron variables are added. See [`configs/single-b-tag/fragments/single-btag-variables-softe.json(TBU)`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/blob/89802b14772da7ccf423efc5a98d183754be96ae/configs/single-b-tag/fragments/single-btag-variables-softe.json#L8) for an example. The 29 electron variables currently have the naming pattern of: 
 ```json
        "hfe_npix_100cut",
```

The current augmenter can add 29 electron variables for 2 phf cuts, where about 80%(90%) of true soft electrons are selected. However, the latest results show that 80%(90%) phf cuts are not helpful ([FTag Plenary 8/2/2022](https://indico.cern.ch/event/1186614/)). Except for cross-checking this conclusion, those additional variables do not need to be included. In addition, the augmenter can add another 9 truth electron variables. However, they are only needed for soft electron development as introduced in the last section. 

To run a local test in the Athena enviroment, run: 
```bash
ca-dump-single-btag -c <path to configuration file> <paths to xAOD(s)>
```

Currently, the TDD training framework is only tested to work with the default R22 mc20a ttbar DAOD (`mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_PHYSVAL.e6337_s3681_r13167_p4931`). Check the [FTAG-doc](https://ftag.docs.cern.ch/software/samples/) for the latest samples recommended. 

To submit a grid job, run 
```bash
grid-submit -s ca-dump-single-btag single-btag
```
This command uses the default config and input for [`single-btag`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/blob/r22/BTagTrainingPreprocessing/grid/grid-submit#L28) but changes the script to `ca-dump-single-btag` for the Athena environment. Note: grid jobs can be 3 times slower with Athena environment compared to the Analysisbase environment. 

### Umami with electrons<a name="Umami with electrons"></a>
After preparing the sample with electron variables in TDD, the electron variables need to be added to the [Umami framework](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/umami) for training. The Umami documentation is very useful for understanding each step in the latest training. This document focuses on the changes needed to soft electrons. In addition, currently, the training for `DL1re` is only done with ttbar samples. All the config file examples are for ttbar samples only. 

For preprocessing, simiply add the 29 electron variables to the preprocessing config file. For example, in [`offline/PFlow-Preprocessing-ttbarOnly-softe.yaml TBU`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/umami-config-tags/-/blob/dc5768484b598f516f7f35abb3729e9e29849ffa/offline/PFlow-Preprocessing-ttbarOnly-softe.yaml), make sure to include the correct parameter config: 
```yaml
parameters: !include Preprocessing-parameters-ttbarOnly-softe.yaml
```
In the [parameter config TBU](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/umami-config-tags/-/blob/dc5768484b598f516f7f35abb3729e9e29849ffa/offline/preprocessing_parameters/Preprocessing-parameters-ttbarOnly-softe.yaml), include the correct variable config: 
```yaml
.var_file: &var_file /global/cfs/cdirs/atlas/jxiong/workdir/hfe/umami-merge/umami/configs/hfe/DL1r_var_hfe_r22.yaml
```
In the [variable config TBU](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/umami-config-tags/-/blob/dc5768484b598f516f7f35abb3729e9e29849ffa/offline/DL1r_Variables_R22_softe.yaml), include all 29 variables added from the dumper such as the variable listed here:  
```yaml
    - hfe_isdefault_100cut
```
As noted above, the other 58 electron variables for 80%(90%) phf cuts do not need to be included in the training-dataset-dumper and Umami training due to worse DL1re performance. The 9 truth electron variables should only be included when doing soft electron development as introduced in the last section. 

For training, if truth electron variables are added for soft electron development. Remember to exclude them as done in [`offline/DL1r-PFlow-Training-config-ttbarOnly-softe-r22.yaml TBU`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/umami-config-tags/-/blob/dc5768484b598f516f7f35abb3729e9e29849ffa/offline/DL1r-PFlow-Training-config-ttbarOnly-softe-r22.yaml). In addition, if additional electron variables are added for different phf cuts, exclude variables from cuts that are not being studied. 

For evaluation and plotting, simiply use the soft electron preprocessing and training configs you used and follow the latest Umami instruction.

### Advanced: soft electron tagging development<a name="advanced"></a>

#### soft electrons in training-dataset-dumper<a name="advanced-tdd"></a>

The electron variables were originally added following the instruction for [augmenting the xAOD objects in TDD](https://training-dataset-dumper.docs.cern.ch/development/#adding-an-augmentation). See [MR1](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/merge_requests/396) for details of the implementation. 

However, as the current electron augmenter only works with the Athena framework. The implementation is modified to be a stand-alone Athena algorithm. See [MR2](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/merge_requests/399) for details of the implementation. 

#### soft electrons in umami<a name="advanced-umami"></a>

To understand the soft electron performance from true and fake soft electrons selected inside the jets, it might be useful to evaluate the efficiencies and rejections for jets with true, fake, and no soft electron candidates. This evaluation is developed for Umami `0.2-189-gfbf3ead` but not included in the main Umami repository. Check [`GetRejectionPerEfficiencyDict_softe`](https://gitlab.cern.ch/jxiong/umami/-/blob/xjw-softe/umami/evaluate_model.py#L643) as an example for modifying the default `GetRejectionPerEfficiencyDict` function to calculate rejections for background jets with true, fake, and no soft electron candidates at various efficiencies for selecting signal jets with true, fake, and no soft electrons. Here, it's important to include in evaluation: 
```yaml
    "hfe_origin_100cut", "hfe_type_100cut", "hfe_isdefault_100cut"
```
to define jets with true, fake, and no soft electrons. Make sure the two truth labels (`"hfe_origin_100cut", "hfe_type_100cut"`) are added in the training-dataset-dumper and Umami preprocessing if needed for this purpose. 

Once the rejections and efficiencies are calculated. Check [`plotROCRatio_softe`](https://gitlab.cern.ch/jxiong/umami/-/blob/xjw-softe/umami/plotting_umami.py#L182) as an example for modifying hte default `plotROCRatio` to plot rejections and efficiencies for jets with true, fake, and no soft electron candidates. An example of the plots can be found at [FTag Plenary 8/2/2022](https://indico.cern.ch/event/1186614/). 

In addition, the `GetRejectionPerEfficiencyDict` function can be modified to calculate the rejections of fake soft electron candidates based on `phf`. Check [`GetRejectionPerEfficiencyDict_only_hfe`](https://gitlab.cern.ch/jxiong/umami/-/blob/xjw-softe/umami/evaluate_model.py#L604) as an example. Similarly, check [`plot_ROC_phf`](https://gitlab.cern.ch/jxiong/umami/-/blob/xjw-softe/umami/plotting_umami.py#L205) as an example for plotting the `phf` performance. 

In [FTag Plenary 8/2/2022](https://indico.cern.ch/event/1186614/), there is also a training done using the `true phf` value to understand the best performance if `phf` is 1 for all true soft electrons and 0 for all fake soft electrons. This `true phf` value is added to the dumper as: 
```yaml
    "hfe_phf_true_100cut"
```
Include this variable in the training-dataset-dumper and Umami training for a similar study.