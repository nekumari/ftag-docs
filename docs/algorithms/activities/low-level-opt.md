# Low Level Algorithm Optimisation

## Small networks for low-level algorithm optimisation

[SV1](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BtaggingSV) and [JetFitter](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTaggingJetFitter) are two low-level [b-tagging algorithms](https://ftag.docs.cern.ch/algorithms/#b-tagging-algorithms) based on the secondary vertexing information inside jets. They produce part of the input variables on which high-level taggers belonging to the [DL1](https://ftag.docs.cern.ch/algorithms/dl1/#dl1) family are trained.
SVKine and JFKine are two small NNs based only on SV1 and JetFitter variables. They have been developed for optimising the standalone performance of the two algorithms, and for having a faster turnaround in their monitoring and validation when changes occur. 

The illustration below shows the performance on a ttbar sample of SVKine and JFKine compared to DIPS and DL1dv00.

![SVKine and JFKine ROC curves compared to DIPS and DL1dv00 ](../../assets/roc_svkine.jpg)

## SVKine

The SVKine algorithm is based on a deep feed-forward neural network, subset of the DL1 algorithm. It uses only the SV1 and kinematc variables. The input features of SVKine in Release 22 are:

- jet kinematics: pt and abs(eta)
- SV1: properties of secondary vertices reconstructed with SV1 low-level algorithm

The properties of the SVKine neural network are shown in the table below.

| property                                   | value                             |
| ------------------------------------------ | --------------------------------- |
| number of input variables                  | 11                                |
| number of hidden layers                    | 4                                 |
| number of nodes [per layer]                | [36, 24, 12, 6]                   |
| activation function of all layers          | ReLu                              |
| batch normalisation                        | all layers                        |
| number of training epochs                  | 200                               |
| learning rate                              | 0.01                              |
| learning rate schedule                     | True                              |
| training minibatch size                    | 15000                             |

??? example "Click to see the code to configurate the training of SVKine in the [umami framework](https://umami-docs.web.cern.ch/)"
    ```yaml linenums="1"
   
    model_name: SVKine
    preprocess_config: examples/PFlow-Preprocessing.yaml

    # Add here a pretrained model to start with.
    # Leave empty for a fresh start
    model_file:

    # Add training file
    train_file: <path>/<to>/<train>/<file>/PFlow-hybrid-resampled_scaled_shuffled.h5

    # Defining templates for the variable cuts
    .variable_cuts_ttbar: &variable_cuts_ttbar
        variable_cuts:
            - pt_btagJes:
                operator: "<="
                condition: 2.5e5

    .variable_cuts_zpext: &variable_cuts_zpext
        variable_cuts:
            - pt_btagJes:
                operator: ">"
                condition: 2.5e5

    # Add validation files
    validation_files:
        ttbar_r22_val:
            path: <path>/<to>/<validation>/<file>/MC20d-inclusive_validation_ttbar_PFlow.h5
            label: "$t\\bar{t}$"
            <<: *variable_cuts_ttbar

        zprime_r22_val:
            path: <path>/<to>/<validation>/<file>/MC20d-inclusive_validation_zprime_PFlow.h5
            label: "Z'ext"
            <<: *variable_cuts_zpext

    test_files:
        ttbar_r22:
            path: <path>/<to>/<test>/<file>/MC20d-inclusive_testing_ttbar_PFlow.h5
            <<: *variable_cuts_ttbar

        zpext_r22:
            path: <path>/<to>/<test>/<file>/MC20d-inclusive_testing_zprime_PFlow.h5
            <<: *variable_cuts_zpext

    # Path to Variable dict used in preprocessing
    var_dict: umami/configs/DL1r_Variables_R22.yaml

    exclude: ["RNNIP","JetFitter","JetFitterSecondaryVertex","IP3D","IP2D"]

    NN_structure:
        # Decide, which tagger is used
        tagger: "dl1"

        # NN Training parameters
        lr: 0.01
        batch_size: 15000
        epochs: 200

        # Number of jets used for training
        # To use all: Fill nothing
        nJets_train:

        # Dropout rate. If = 0, dropout is disabled
        dropout: 0

        # Define which classes are used for training
        # These are defined in the global_config
        class_labels: ["ujets", "cjets", "bjets"]

        # Main class which is to be tagged
        main_class: "bjets"

        # Decide if Batch Normalisation is used
        Batch_Normalisation: False

        # Nodes per dense layer. Starting with first dense layer.
        dense_sizes: [36, 24, 12, 6]

        # Activations of the layers. Starting with first dense layer.
        activations: ["relu", "relu", "relu", "relu"]

        # Variables to repeat in the last layer (example)
        repeat_end: ["pt_btagJes", "absEta_btagJes"]

        # Options for the Learning Rate reducer
        LRR: True

        # Option if you want to use sample weights for training
        use_sample_weights: False

    # Plotting settings for training metrics plots
    Validation_metrics_settings:
        # Define which taggers should also be plotted
        taggers_from_file: ["DL1dv00","dipsLoose20210729"]

        # Label for the freshly trained tagger
        tagger_label: "SVKine"

        # Enable/Disable atlas tag
        UseAtlasTag: True

        # fc_value and WP_b are autmoatically added to the plot label
        AtlasTag: "Internal Simulation"
        SecondTag: "\n$\\sqrt{s}=13$ TeV, PFlow jets"

        # Set the datatype of the plots
        plot_datatype: "jpg"

    # Eval parameters for validation evaluation while training
    Eval_parameters_validation:
        # Number of jets used for validation
        n_jets: 3e5
        # Define taggers that are used for comparison in evaluate_model
        # This can be a list or a string for only one tagger
        tagger: ["dipsLoose20210729", "DL1dv00"]

        # Define fc values for the taggers
        frac_values_comp: {
            "dipsLoose20210729": {
                "cjets": 0.005,
                "ujets": 0.995,
            },
            "DL1dv00": {
                "cjets": 0.018,
                "ujets": 0.982,
            },
        }

        # Charm fraction value used for evaluation of the trained model
        frac_values: {
            "cjets": 0.018,
            "ujets": 0.982,
     }

        # A list to add available variables to the evaluation files
        add_variables_eval: ["actualInteractionsPerCrossing"]

        # Working point used in the evaluation
        WP: 0.77

        # some properties for the feature importance explanation with SHAPley
        shapley:
            # Over how many full sets of features it should calculate over.
            # Corresponds to the dots in the beeswarm plot.
            # 200 takes like 10-15 min for DL1r on a 32 core-cpu
            feature_sets: 200

            # defines which of the model outputs (flavor) you want to explain
            # [tau,b,c,u] := [3, 2, 1, 0]
            model_output: 2

            # You can also choose if you want to plot the magnitude of feature
            # importance for all output nodes (flavors) in another plot. This
            # will give you a bar plot of the mean SHAP value magnitudes.
            bool_all_flavor_plot: False

            # as this takes much longer you can average the feature_sets to a
            # smaller set, 50 is a good choice for DL1r
            averaged_sets: 50

            # [11,11] works well for dl1r
            plot_size: [11, 11]

    ```


## JFKine

The JFKine algorithm is based on a deep feed-forward neural network, subset of the DL1 algorithm. It uses only the JetFitter and kinematc variables. The input features of SVKine in Release 22 are:

- jet kinematics: pt and abs(eta)
- JetFitter: properties of secondary vertices reconstructed with JetFitter low-level algorithm
- JetFitter c-tagging: additional properties of secondary vertices reconstructed with JetFitter low-level algorithm used for identification of charm jets

The properties of the JFKine neural network are shown in the table below.

| property                                   | value                             |
| ------------------------------------------ | --------------------------------- |
| number of input variables                  | 24                                |
| number of hidden layers                    | 4                                 |
| number of nodes [per layer]                | [36, 24, 12, 6]                   |
| activation function of all layers          | ReLu                              |
| batch normalisation                        | all layers                        |
| number of training epochs                  | 200                               |
| learning rate                              | 0.01                             |
| learning rate schedule                     | True                              |
| training minibatch size                    | 15000                             |

??? example "Click to see the code to configurate the training of JFKine in the [umami framework](https://umami-docs.web.cern.ch/)"
    ```yaml linenums="1"
   
    model_name: JFKine
    preprocess_config: examples/PFlow-Preprocessing.yaml

    # Add here a pretrained model to start with.
    # Leave empty for a fresh start
    model_file:

    # Add training file
    train_file: <path>/<to>/<train>/<file>/PFlow-hybrid-resampled_scaled_shuffled.h5

    # Defining templates for the variable cuts
    .variable_cuts_ttbar: &variable_cuts_ttbar
        variable_cuts:
            - pt_btagJes:
                operator: "<="
                condition: 2.5e5

    .variable_cuts_zpext: &variable_cuts_zpext
        variable_cuts:
            - pt_btagJes:
                operator: ">"
                condition: 2.5e5

    # Add validation files
    validation_files:
        ttbar_r22_val:
            path: <path>/<to>/<validation>/<file>/MC20d-inclusive_validation_ttbar_PFlow.h5
            label: "$t\\bar{t}$"
            <<: *variable_cuts_ttbar

        zprime_r22_val:
            path: <path>/<to>/<validation>/<file>/MC20d-inclusive_validation_zprime_PFlow.h5
            label: "Z'ext"
            <<: *variable_cuts_zpext

    test_files:
        ttbar_r22:
            path: <path>/<to>/<test>/<file>/MC20d-inclusive_testing_ttbar_PFlow.h5
            <<: *variable_cuts_ttbar

        zpext_r22:
            path: <path>/<to>/<test>/<file>/MC20d-inclusive_testing_zprime_PFlow.h5
            <<: *variable_cuts_zpext

    # Path to Variable dict used in preprocessing
    var_dict: umami/configs/DL1r_Variables_R22.yaml

    exclude: ["RNNIP","SV1","IP3D","IP2D"]

    NN_structure:
        # Decide, which tagger is used
        tagger: "dl1"

        # NN Training parameters
        lr: 0.01
        batch_size: 15000
        epochs: 200

        # Number of jets used for training
        # To use all: Fill nothing
        nJets_train:

        # Dropout rate. If = 0, dropout is disabled
        dropout: 0

        # Define which classes are used for training
        # These are defined in the global_config
        class_labels: ["ujets", "cjets", "bjets"]

        # Main class which is to be tagged
        main_class: "bjets"

        # Decide if Batch Normalisation is used
        Batch_Normalisation: False

        # Nodes per dense layer. Starting with first dense layer.
        dense_sizes: [36, 24, 12, 6]

        # Activations of the layers. Starting with first dense layer.
        activations: ["relu", "relu", "relu", "relu"]

        # Variables to repeat in the last layer (example)
        repeat_end: ["pt_btagJes", "absEta_btagJes"]

        # Options for the Learning Rate reducer
        LRR: True

        # Option if you want to use sample weights for training
        use_sample_weights: False

    # Plotting settings for training metrics plots
    Validation_metrics_settings:
        # Define which taggers should also be plotted
        taggers_from_file: ["DL1dv00","dipsLoose20210729"]

        # Label for the freshly trained tagger
        tagger_label: "JFKine"

        # Enable/Disable atlas tag
        UseAtlasTag: True

        # fc_value and WP_b are autmoatically added to the plot label
        AtlasTag: "Internal Simulation"
        SecondTag: "\n$\\sqrt{s}=13$ TeV, PFlow jets"

        # Set the datatype of the plots
        plot_datatype: "jpg"

    # Eval parameters for validation evaluation while training
    Eval_parameters_validation:
        # Number of jets used for validation
        n_jets: 3e5
        # Define taggers that are used for comparison in evaluate_model
        # This can be a list or a string for only one tagger
        tagger: ["dipsLoose20210729", "DL1dv00"]

        # Define fc values for the taggers
        frac_values_comp: {
            "dipsLoose20210729": {
                "cjets": 0.005,
                "ujets": 0.995,
            },
            "DL1dv00": {
                "cjets": 0.018,
                "ujets": 0.982,
            },
        }

        # Charm fraction value used for evaluation of the trained model
        frac_values: {
            "cjets": 0.018,
            "ujets": 0.982,
     }

        # A list to add available variables to the evaluation files
        add_variables_eval: ["actualInteractionsPerCrossing"]

        # Working point used in the evaluation
        WP: 0.77

        # some properties for the feature importance explanation with SHAPley
        shapley:
            # Over how many full sets of features it should calculate over.
            # Corresponds to the dots in the beeswarm plot.
            # 200 takes like 10-15 min for DL1r on a 32 core-cpu
            feature_sets: 200

            # defines which of the model outputs (flavor) you want to explain
            # [tau,b,c,u] := [3, 2, 1, 0]
            model_output: 2

            # You can also choose if you want to plot the magnitude of feature
            # importance for all output nodes (flavors) in another plot. This
            # will give you a bar plot of the mean SHAP value magnitudes.
            bool_all_flavor_plot: False

            # as this takes much longer you can average the feature_sets to a
            # smaller set, 50 is a good choice for DL1r
            averaged_sets: 50

            # [11,11] works well for dl1r
            plot_size: [11, 11]

    ```


# Soft Electron tagger

## DL1re (R22, ttbarOnly, last update 7/29/2022)

Since 20%(10%) of $b(c)$-hadrons have electrons in their decay, the previous/current recommended flavour tagging algorithm, DL1r/d, might benefit from electrons from heavy flavor decays. The current `DL1re` tagger includes 29 electron variables for training. The electron with the highest `phf` value, the probability of being an electron from the heavy flavour decays, is selected as the soft electron candidate inside the jets. `phf` cuts for the soft electron candidates are not recommended. The soft electron tagger is being summarised in a [note (very preliminary draft)](https://cernbox.cern.ch/index.php/apps/files/?dir=/workdir/softe/doc&#pdfviewer). Check the document for a detailed discussion of all the variables added. The soft electron presentations in the ftag meetings are summarised below.

This document summarises how to add the 29 soft electrons to DL1r and check the improved background jet rejections in practice. The process can be divided to two steps: 
- [First, include electron variables in the training-dataset-dumper](#TDD with electrons)
- [Second, include electron variables in the Umami training](#Umami with electrons)

In addition, the last section summarises some tips for future development of the soft electron tagger: 
- [Advanced: soft electron tagging development](#advanced)
    - [soft electrons in training-dataset-dumper](#advanced-tdd)
    - [soft electrons in Umami](#advanced-umami)

### training-dataset-dumper with electrons<a name="TDD with electrons"></a>
The current modification for SET is based on the [official r22 training-dataset-dumper (TDD) framework](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper), whose documentation explains in details how to install and run the dumper to create training samples from DAODs. This document focuses on the changes needed to soft electrons. 

To add the electron variables to the training samples, setup up the Athena environment instead of the Analaysisbase environment:
```bash
git clone ssh://git@gitlab.cern.ch:7999/atlas-flavor-tagging-tools/training-dataset-dumper.git
source training-dataset-dumper/setup-athena.sh
mkdir build
cd build
cmake ../training-dataset-dumper
make
cd ..
source build/x*/setup.sh
```
The current electron augmenter uses GaudiKernel from Athena for calculating electron ID DNN scores as the DNN scores are not stored in the default PHYSVAL DAOD for flavor tagging. Check the [electron augmenter MR1](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/merge_requests/396) for the discussion. 

After compiling, to add electron variables, set:  
```json
        "hfe": true
```
See [`configs/single-b-tag/EMPFlowSlim_softe.json(TBU)`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/blob/89802b14772da7ccf423efc5a98d183754be96ae/configs/single-b-tag/EMPFlowSlim_softe.json#L62) for an example. 

Make sure all 29 electron variables are added. See [`configs/single-b-tag/fragments/single-btag-variables-softe.json(TBU)`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/blob/89802b14772da7ccf423efc5a98d183754be96ae/configs/single-b-tag/fragments/single-btag-variables-softe.json#L8) for an example. The 29 electron variables currently have the naming pattern of: 
 ```json
        "hfe_npix_100cut",
```

The current augmenter can add 29 electron variables for 2 phf cuts, where about 80%(90%) of true soft electrons are selected. However, the latest results show that 80%(90%) phf cuts are not helpful ([FTag Plenary 8/2/2022](https://indico.cern.ch/event/1186614/)). Except for cross-checking this conclusion, those additional variables do not need to be included. In addition, the augmenter can add another 9 truth electron variables. However, they are only needed for soft electron development as introduced in the last section. 

To run a local test in the Athena enviroment, run: 
```bash
ca-dump-single-btag -c <path to configuration file> <paths to xAOD(s)>
```

Currently, the TDD training framework is only tested to work with the default R22 mc20a ttbar DAOD (`mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_PHYSVAL.e6337_s3681_r13167_p4931`). Check the [FTAG-doc](https://ftag.docs.cern.ch/software/samples/) for the latest samples recommended. 

To submit a grid job, run 
```bash
grid-submit -s ca-dump-single-btag single-btag
```
This command uses the default config and input for [`single-btag`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/blob/r22/BTagTrainingPreprocessing/grid/grid-submit#L28) but changes the script to `ca-dump-single-btag` for the Athena environment. Note: grid jobs can be 3 times slower with Athena environment compared to the Analysisbase environment. 

### Umami with electrons<a name="Umami with electrons"></a>
After preparing the sample with electron variables in TDD, the electron variables need to be added to the [Umami framework](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/umami) for training. The Umami documentation is very useful for understanding each step in the latest training. This document focuses on the changes needed to soft electrons. In addition, currently, the training for `DL1re` is only done with ttbar samples. All the config file examples are for ttbar samples only. 

For preprocessing, simiply add the 29 electron variables to the preprocessing config file. For example, in [`offline/PFlow-Preprocessing-ttbarOnly-softe.yaml TBU`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/umami-config-tags/-/blob/dc5768484b598f516f7f35abb3729e9e29849ffa/offline/PFlow-Preprocessing-ttbarOnly-softe.yaml), make sure to include the correct parameter config: 
```yaml
parameters: !include Preprocessing-parameters-ttbarOnly-softe.yaml
```
In the [parameter config TBU](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/umami-config-tags/-/blob/dc5768484b598f516f7f35abb3729e9e29849ffa/offline/preprocessing_parameters/Preprocessing-parameters-ttbarOnly-softe.yaml), include the correct variable config: 
```yaml
.var_file: &var_file /global/cfs/cdirs/atlas/jxiong/workdir/hfe/umami-merge/umami/configs/hfe/DL1r_var_hfe_r22.yaml
```
In the [variable config TBU](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/umami-config-tags/-/blob/dc5768484b598f516f7f35abb3729e9e29849ffa/offline/DL1r_Variables_R22_softe.yaml), include all 29 variables added from the dumper such as the variable listed here:  
```yaml
    - hfe_isdefault_100cut
```
As noted above, the other 58 electron variables for 80%(90%) phf cuts do not need to be included in the training-dataset-dumper and Umami training due to worse DL1re performance. The 9 truth electron variables should only be included when doing soft electron development as introduced in the last section. 

For training, if truth electron variables are added for soft electron development. Remember to exclude them as done in [`offline/DL1r-PFlow-Training-config-ttbarOnly-softe-r22.yaml TBU`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/umami-config-tags/-/blob/dc5768484b598f516f7f35abb3729e9e29849ffa/offline/DL1r-PFlow-Training-config-ttbarOnly-softe-r22.yaml). In addition, if additional electron variables are added for different phf cuts, exclude variables from cuts that are not being studied. 

For evaluation and plotting, simiply use the soft electron preprocessing and training configs you used and follow the latest Umami instruction.

### Advanced: soft electron tagging development<a name="advanced"></a>

#### soft electrons in training-dataset-dumper<a name="advanced-tdd"></a>

The electron variables were originally added following the instruction for [augmenting the xAOD objects in TDD](https://training-dataset-dumper.docs.cern.ch/development/#adding-an-augmentation). See [MR1](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/merge_requests/396) for details of the implementation. 

However, as the current electron augmenter only works with the Athena framework. The implementation is modified to be a stand-alone Athena algorithm. See [MR2](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/merge_requests/399) for details of the implementation. 

#### soft electrons in umami<a name="advanced-umami"></a>

To understand the soft electron performance from true and fake soft electrons selected inside the jets, it might be useful to evaluate the efficiencies and rejections for jets with true, fake, and no soft electron candidates. This evaluation is developed for Umami `0.2-189-gfbf3ead` but not included in the main Umami repository. Check [`GetRejectionPerEfficiencyDict_softe`](https://gitlab.cern.ch/jxiong/umami/-/blob/xjw-softe/umami/evaluate_model.py#L643) as an example for modifying the default `GetRejectionPerEfficiencyDict` function to calculate rejections for background jets with true, fake, and no soft electron candidates at various efficiencies for selecting signal jets with true, fake, and no soft electrons. Here, it's important to include in evaluation: 
```yaml
    "hfe_origin_100cut", "hfe_type_100cut", "hfe_isdefault_100cut"
```
to define jets with true, fake, and no soft electrons. Make sure the two truth labels (`"hfe_origin_100cut", "hfe_type_100cut"`) are added in the training-dataset-dumper and Umami preprocessing if needed for this purpose. 

Once the rejections and efficiencies are calculated. Check [`plotROCRatio_softe`](https://gitlab.cern.ch/jxiong/umami/-/blob/xjw-softe/umami/plotting_umami.py#L182) as an example for modifying hte default `plotROCRatio` to plot rejections and efficiencies for jets with true, fake, and no soft electron candidates. An example of the plots can be found at [FTag Plenary 8/2/2022](https://indico.cern.ch/event/1186614/). 

In addition, the `GetRejectionPerEfficiencyDict` function can be modified to calculate the rejections of fake soft electron candidates based on `phf`. Check [`GetRejectionPerEfficiencyDict_only_hfe`](https://gitlab.cern.ch/jxiong/umami/-/blob/xjw-softe/umami/evaluate_model.py#L604) as an example. Similarly, check [`plot_ROC_phf`](https://gitlab.cern.ch/jxiong/umami/-/blob/xjw-softe/umami/plotting_umami.py#L205) as an example for plotting the `phf` performance. 

In [FTag Plenary 8/2/2022](https://indico.cern.ch/event/1186614/), there is also a training done using the `true phf` value to understand the best performance if `phf` is 1 for all true soft electrons and 0 for all fake soft electrons. This `true phf` value is added to the dumper as: 
```yaml
    "hfe_phf_true_100cut"
```
Include this variable in the training-dataset-dumper and Umami training for a similar study. 


# Soft Muon Tagger

As of 01.07.22 there is an [internal note](https://cds.cern.ch/record/2814375) that documented the latest progress on the Soft Muon Tagger (SMT) and can also serve as an introduction to the topic. The soft muon variables are written to the jet with the BTagMuonAugmenter [here](https://gitlab.cern.ch/atlas/athena/-/blob/master/PhysicsAnalysis/JetTagging/FlavorTagDiscriminants/Root/BTagMuonAugmenter.cxx). Formerly the SMT added the outputs of a [standalone neural network](https://gitlab.cern.ch/rateixei/munn) to the inputs of the higher level taggers like DL1r. Studies showed that the performance is basically the same as using the soft muon variables as direct inputs into DL1r. It was decided to default to this option as it reduces interdependencies and DL1r then gets the name DL1rmu. The SMT was also tested within DL1d and resulted in a 25% improvement for light flavor jet rejection and a 20% improvement for c flavor jet rejection at a b-tagging efficiency of 77%. A lot of additional muon variables were explored but did not gain any significant improvements (cf. [internal note](https://cds.cern.ch/record/2814375)). As the associated muons are not required to fullfill any identification working point the Muon CP group recommended to apply their ["low $p_t$" identification working point](https://cds.cern.ch/record/2710574). Two options can be explored with this additional info. Either by using another a boolean variable along with the soft muon variables or by masking the soft muon variables for any muon that did not pass the identification working point. Both ways perform in a similar fashion and increases the performance of the light flavor rejection of DL1dmu by another ~15% and the c-flavor rejection by a few percent at a 77% b-tagging efficiency. 

Fig. 1 shows DL1dmu training with different input vars evaluated on $t\bar{t}$. Explored were additional hits variables, additional variables from the Muon_v1.h and the "low $p_t$" identification working point variable.

![Fig.1](../../assets/DL1dmu_ttbar.png)

Fig. 2 shows the same as Fig. 1 but evaluated on $Z'$
![Fig.2](../../assets/DL1dmu_zpext.png)

There is a corrected version of ptrel available now, that needs to be checked how it affects the performance.

