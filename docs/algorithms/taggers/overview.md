# FTAG Algorithms (aka "Taggers")

This section present the main algorithm chain for ATLAS flavour tagging developed and supported by the Flavour Tagging group for release 22 used in Run-3 physics analysis, as well as for the legacy releases 20.7 and 21 for Run-2 physics analysis. Ongoing activities on additional optimisation studies on the current b-tagging configuration or on novel taggers are listed below.

## Run-3 algorithms

The algorithms developed for the analysis of Run-3 physics analysis are the graph-neural network based GN2 and GN1 algorithms and the DL1d algorithm, which follows the traditional two-staged approach using low-level and high-level algorithms.

![Overview of the Run-2 and Run-3 algorithm approach](../../assets/run3-algos.png)


- [GN2][gn2-tagger]
- [GN1][gn1-tagger]
- [DL1d][dl1-tagger]

## Run-2 algorithms

In the analysis of Run-2 data a two-staged approach using low-level and high-level algorithms has been used.
The relation between low-level algorithms and high-level algorithms is illustrated below.

![Overview of low-level and high-level algorithms](../../assets/taggers_overview.png)

**Documentation for low-level taggers:**

- [IPTag](https://gitlab.cern.ch/fdibello/IPtagTuning)
- [RNNIP](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTaggingRNNIP)
- [DIPS](dips.md)
- [SV](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BtaggingSV)
- [JetFitter](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTaggingJetFitter)
- [SMT](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTaggingSMT)

**Documentation for high-level taggers:**

- [DL1r](dl1.md)
- [b-tagging MV2](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTaggingMV2)
- [c-tagging MV2](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/CTaggingMV2)
