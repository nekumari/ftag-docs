This page contains information about available ONNX-based taggers stored in the FTAG group space.
Model files are first copied to the EOS space by the group conveners, and these files are then automatically replicated to CVMFS.
The relevant paths can be found below.

??? info "Paths to the Group Space"

    === "EOS"
        Base path on EOS:
        ```
        /eos/atlas/atlascerngroupdisk/asg-calib/
        ```

        === "FTAG Group Space EOS"
            Group space EOS:
            ```
            /eos/atlas/atlascerngroupdisk/asg-calib/BTagging/
            ```
            Web interface: [https://atlas-groupdata.web.cern.ch/atlas-groupdata/BTagging/](https://atlas-groupdata.web.cern.ch/atlas-groupdata/BTagging/)

        === "FTAG Dev Space EOS"
            Dev space EOS:
            ```
            /eos/atlas/atlascerngroupdisk/asg-calib/dev/BTagging/
            ```
            Web interface: [https://atlas-groupdata.web.cern.ch/atlas-groupdata/dev/BTagging/](https://atlas-groupdata.web.cern.ch/atlas-groupdata/dev/BTagging/)

    === "CVMFS"
        Base path on CVMFS:
        ```
        /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/
        ```

        === "FTAG Group Space CVMFS"
            Group space CVMFS:
            ```
            /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/BTagging/
            ```

        === "FTAG Dev Space CVMFS"
            Dev space CVMFS:
            ```
            /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/BTagging/
            ```

### How is this page generated?

This page is generated in two stages in the docs CI pipeline:

1. [This script]({{repo_url}}-/blob/master/deploy_tools/dump-metadata.sh) dumps json metadata for all lwtnn and onnx models found in the group space to an [eos site](https://umami-tagger-metadata.web.cern.ch/). The script runs with the help of the [`EOS Runners`](https://gitlab.docs.cern.ch/docs/Build%20your%20application/CI-CD/Runners/cvmfs-eos-runners/) to access the `umami` service account.
2. [A second script]({{repo_url}}-/blob/master/deploy_tools/tagger_metadata.py) builds this page from the json files stored on eos.


??? info "Some taggers are missing information"

    Depending on what information is available in the metadata, some taggers may not have all the information available.
    Generally, newer taggers will have more information available that older taggers.
    We are working to increase the available metadata for taggers going forward.


## Available Taggers

