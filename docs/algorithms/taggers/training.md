# Training

This page is intended to give an overview of how to go about training taggers.

???+info "[Software tutorials](../../software/tutorials/index.md)"

    The FTAG group maintains several software tutorials for learning about the different software packages used.
    These will give you a foundational understanding of the software you will be using for training.
    You can find them [here](../../software/tutorials/index.md).

???+info "Other useful links"

    - [Overview of algo software](../../software/algorithms-software.md)
    - [Information about model architectures](overview.md)
    - [Information about training inputs](inputs.md)
    - [Information about jet labels](../labelling/jet_labels.md)
    - [Information about track labels](../labelling/track_labels.md)
    - [Deploying a tagger in Athena](deploy.md)
    - [GPU access at ATLAS](https://atlasml.web.cern.ch/atlasml/resources/hardware/)


The full training workflow is outlined below, although note that you likely don't need to start from the beginning for your use case.
Working out how far down the chain you can start is crucial to getting started quickly.
If you're not sure, please just ask for help.

1. FTAG1 derivation production (see [here](../../samples/daod.md))
2. H5 production (using the [FTAG dumpster](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/))
3. Preprocessing (using [UPP](https://github.com/umami-hep/umami-preprocessing))
4. Training (using [Salt](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/salt))


## Samples

You have two options for samples to use for training:

1. Use the preprocessed samples provided by the flavour tagging group.
2. Produce your own training samples.

Please keep always track of which exact sample you were using for your studies.

### Using provided training samples

Using existing preprocessed inputs is the easiest way to get started with training.
These inputs are ready to use for training with Salt without additional steps.
However, you should be aware that the preprocessed samples are not always up-to-date with the latest version of the software, and if you want to make modifications to the preprocessing or training, you may need to run the full chain yourself.

The latest preprocessed samples are those used for the GN2v01 training.
These are available on EOS for the 3rd fold only.
The sample is split into 30M jet chunks due to limitations on files sizes in EOS.

```
/eos/atlas/atlascerngroupdisk/perf-flavtag/training/training_gn2_fold3_20231205_mc20mc23_combined_270M/
```

!!!info "You can train on more than 30M jets by merging several chunks into a virtual file using the steps [here](https://github.com/umami-hep/atlas-ftag-tools?tab=readme-ov-file#create-virtual-file)."


### Producing your own training samples

Producing your own training samples can mean different things, depending on how far back up the chain you start from.
If you don't need to re-run derivation production or H5 dumping, you can use the centrally produced H5 dumps.
A list of these samples is available [here](../../samples/samples.md).
Please make sure you use the most recent H5 samples listed, as changes are happening frequently.

## Preprocessing

See [the UPP docs](https://umami-hep.github.io/umami-preprocessing/), or take a look at the [preprocessing tutorial](../../software/tutorials/tutorial-upp.md).

## Training

See [the Salt docs](https://ftag-salt.docs.cern.ch/), or take a look at the [salt tutorial](https://ftag-salt.docs.cern.ch/tutorial/).
