# Using different MC event generators

Because different MC event generators produce differing amounts of flavour-tagging observables, for example, different production rates of various heavy-flavoured mesons, this difference needs to be accounted for if you use another MC event generator from the "default" one that was used in the calibration analyses.

This is fairly simple to take into account, by applied so-called **MC-MC scale-factors**; more info on these can be found at the [MC/MC efficiency maps](https://ftag-docs.docs.cern.ch/algorithms/activities/mcmc/) page. Very basically, they are defined as the *ratio of tagger efficiency between the default and the different MC event generators*, i.e.

$$
SF_{MC_2} = \frac{\epsilon_{b}^{MC_1}}{\epsilon_{b}^{MC_2}} 
$$

where $MC_1$ refers to the default MC event generator, and $MC_2$ is the event generator of your analysis. This additional scale-factor is applied as a correction in the computation of the jet-weights mentioned in the previous section. For a b-tagged jet, the weight becomes 

$$
SF_{MC_2}(p_{T}) = \frac{\epsilon_{b}^{data} (p_{T})}{ \epsilon_{b}^{MC_1} (p_{T})} \times \frac{\epsilon_{b}^{MC_1}(p_{T})}{\epsilon_{b}^{MC_2}(p_{T})} = \frac{\epsilon_{b}^{data}(p_{T})}{\epsilon_{b}^{MC_2}(p_{T})}
$$


and the inefficiency scale-factor is likewise corrected as 

$$
w_{jet} = \frac{1 - SF_{MC_2}(p_{pT})\epsilon_{b}^{MC_2} }{1 - \epsilon_{b}^{MC_2}}
$$
