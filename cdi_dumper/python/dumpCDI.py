from os import makedirs
from os.path import join

import yaml
from ROOT import TFile


def dumpCDITaggers(cdi_file, config):
    taggers = {}
    for key in cdi_file.GetListOfKeys():
        if key.GetName() == "VersionInfo":
            continue
        tagger = key.GetName()
        taggers[tagger] = {}
        tagdir = cdi_file.Get(tagger)
        jetCols = tagdir.GetListOfKeys()
        for jetcol in [key.GetName() for key in jetCols]:
            jetcoldir = tagdir.Get(jetcol)
            taggers[tagger][jetcol] = {}
            for workingPoint in [key.GetName() for key in jetcoldir.GetListOfKeys()]:
                wpdir = jetcoldir.Get(workingPoint)
                taggers[tagger][jetcol][workingPoint] = {}
                taggers[tagger][jetcol][workingPoint]["flavours"] = {}
                for flav in [key.GetName() for key in wpdir.GetListOfKeys()]:
                    if flav in ["cutvalue", "fraction", "fraction_tau"]:
                        taggers[tagger][jetcol][workingPoint][flav] = (wpdir.Get(flav)).GetMatrixArray()[0]
                        continue
                    elif flav in config["flavours"]:
                        taggers[tagger][jetcol][workingPoint]["flavours"][flav] = {}

                        # MC / MC efficiency maps ingredients
                        try:
                            mc_had_ref = wpdir.Get(flav + "/MChadronisation_ref")
                            taggers[tagger][jetcol][workingPoint]["flavours"][flav]["mc_had_ref"] = {}
                            # ROOT has an abominable way of iterating through its collections
                            it = mc_had_ref.MakeIterator()
                            for i in range(mc_had_ref.GetEntries()):
                                it_key = it.Next().GetName()
                                it_val = mc_had_ref.GetValue(it_key).GetName()
                                taggers[tagger][jetcol][workingPoint]["flavours"][flav]["mc_had_ref"][it_key] = it_val
                        except ReferenceError:
                            taggers[tagger][jetcol][workingPoint]["flavours"][flav]["mc_had_ref"] = None

                        # default scale factor name
                        try:
                            defaultSF = wpdir.Get(flav + "/default_SF")
                            taggers[tagger][jetcol][workingPoint]["flavours"][flav]["default_SF"] = defaultSF.GetName()
                        except ReferenceError:
                            taggers[tagger][jetcol][workingPoint]["flavours"][flav]["default_SF"] = None

                        # names of all scale factor objects and MC efficiency maps
                        maplist = []
                        sfobjectlist = []
                        flavdir = wpdir.Get(flav)
                        for objectlist in flavdir.GetListOfKeys():
                            if "Eff" in objectlist.GetName():
                                maplist.append(str(objectlist.GetName()))
                            if "SF" in objectlist.GetName():
                                sfobjectlist.append(str(objectlist.GetName()))

                        taggers[tagger][jetcol][workingPoint]["flavours"][flav]["maplist"] = maplist
                        taggers[tagger][jetcol][workingPoint]["flavours"][flav]["sflist"] = sfobjectlist
    return taggers


def main():
    with open(join("data", "config.yaml")) as file:
        config = yaml.load(file, Loader=yaml.FullLoader)

    # dump CI info
    data = {}
    cdi_basepath = config["cdi_basepath"]
    for cdi in config["cdi_files"]:
        cdi_path = join(cdi_basepath, cdi)
        print(f"Dumping {cdi}")
        cdi_file = TFile(cdi_path, "r")
        data[cdi] = {}
        data[cdi]["taggers"] = dumpCDITaggers(cdi_file, config)
        data[cdi]["cdi_path"] = cdi_path

    makedirs(join("output", "yaml"), exist_ok=True)
    with open(join("output", "yaml", "cdi_content.yaml"), "w") as file:
        yaml.dump(data, file, default_flow_style=False)


if __name__ == "__main__":
    main()
