std::vector<float> getbtaginfo(std::string cdiFile, std::string tagger, std::string workingpoint, std::string jetcol, std::string sfObject, float pt_low, float pt_high) {

  gErrorIgnoreLevel = kWarning;
  gROOT->SetBatch(1);
  std::vector<float> effs;

  xAOD::TStore store;
  CP::CorrectionCode::enableFailure();

  // hard-coded settings
  std::vector<int> flavors = {0,4,5,15};
  std::map<int, std::string> flavor_map = {{0, "Light"}, {4, "C"}, {5, "B"}, {15, "T"}}; 

  for (uint i = 0; i < flavors.size(); i++) {
    BTaggingEfficiencyTool* tool;
    tool = new BTaggingEfficiencyTool("BTagTest");
    if (StatusCode::SUCCESS != tool->setProperty("ScaleFactorFileName", cdiFile)     ){ std::cerr << "error initializing tool " << std::endl; return effs;}
    if (StatusCode::SUCCESS != tool->setProperty("TaggerName",          tagger)      ){ std::cerr << "error initializing tool " << std::endl; return effs;}
    if (StatusCode::SUCCESS != tool->setProperty("OperatingPoint",      workingpoint)){ std::cerr << "error initializing tool " << std::endl; return effs;}
    if (StatusCode::SUCCESS != tool->setProperty("JetAuthor",           jetcol)      ){ std::cerr << "error initializing tool " << std::endl; return effs;}
    if (StatusCode::SUCCESS != tool->setProperty("MinPt",               pt_low)      ){ std::cerr << "error initializing tool " << std::endl; return effs;}
    if (StatusCode::SUCCESS != tool->setProperty("VerboseCDITool",      false)       ){ std::cerr << "error initializing tool " << std::endl; return effs;}
    std::string flavor_label = flavor_map.at(flavors.at(i)); 
    std::string toolName = "Efficiency" + flavor_label + "Calibrations";
    if (StatusCode::SUCCESS != tool->setProperty(toolName, sfObject)){ std::cerr << "error initializing tool " << std::endl; return effs;  }
    tool->setProperty("OutputLevel", MSG::FATAL).ignore();
    StatusCode code = tool->initialize();

    if (code != StatusCode::SUCCESS) {
      std::cerr << "Initialization of tool " << tool->name() << " failed! " << std::endl;
      return effs;
    }

    Analysis::CalibrationDataVariables var;
    Analysis::Uncertainty uncertainty = Analysis::Total;
    var.jetAuthor = jetcol;
    float eff;

    // obtain MC efficiencies for pt_low and pt_high
    var.jetPt = pt_low*1000;   
    var.jetEta = 1.0;
    tool->getMCEfficiency(flavors.at(i), var, eff).ignore();
    effs.push_back(eff);    

    var.jetPt = pt_high*1000;   
    var.jetEta = 1.0;
    tool->getMCEfficiency(flavors.at(i), var, eff).ignore();
    effs.push_back(eff);    
  
    // clean up
    delete tool;
  }

  return effs;
}
