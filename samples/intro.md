The FTAG group uses H5 datasets produced from FTAG1 deriavtions for training and evaluation of algorithms. When performing such studies, please use the recommended samples listed below to avoid using something out of date.

Please keep always track of which exact sample you were using for your studies.

!!!info "Ugrade samples are found [here](../algorithms/activities/upgrade.md) and trackless samples are found [here](../algorithms/activities/trackless.md)"
